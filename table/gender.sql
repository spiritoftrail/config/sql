CREATE TABLE gender (
    id VARCHAR(1) NOT NULL,      -- Identifiant
    name VARCHAR(6) NOT NULL,    -- Valeur
    PRIMARY KEY(id)
);

INSERT INTO gender (id, name) VALUES ('M', 'Male');
INSERT INTO gender (id, name) VALUES ('F', 'Female');

