CREATE TABLE level (
    id VARCHAR(1) NOT NULL,      -- Identifiant
    name VARCHAR(8) NOT NULL,    -- Valeur
    PRIMARY KEY(id)
);

INSERT INTO level (id, name) VALUES ('V', 'Visiteur');
INSERT INTO level (id, name) VALUES ('M', 'Membre');
INSERT INTO level (id, name) VALUES ('E', 'Editeur');
INSERT INTO level (id, name) VALUES ('A', 'Admin');

